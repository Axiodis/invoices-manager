package models;

public enum InvoiceStatus {
	PENDING, APPROVED, REJECTED
}
