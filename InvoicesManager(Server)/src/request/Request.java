package request;


public class Request implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RequestType type;
	private Object content;
	private Message message;
	
	public Request(RequestType type) {
		super();
		this.type = type;
		this.content = null;
		this.message = null;
	}
	
	public Request(RequestType type, Object content) {
		super();
		this.type = type;
		this.content = content;
		this.message = null;
	}

	public Request(RequestType type, Object content, Message message) {
		super();
		this.type = type;
		this.content = content;
		this.message = message;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public RequestType getType() {
		return type;
	}

	public void setType(RequestType type) {
		this.type = type;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}
	
}
