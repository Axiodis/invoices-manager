package request;

public class Message implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MessageType type;
	String mainMessage;	
	String headerMessage;
	
	public Message(MessageType type, String mainMessage) {
		super();
		this.type = type;
		this.mainMessage = mainMessage;
	}
	
	
	public Message(MessageType type, String mainMessage, String headerMessage) {
		super();
		this.type = type;
		this.mainMessage = mainMessage;
		this.headerMessage = headerMessage;
	}


	public MessageType getType() {
		return type;
	}
	public void setType(MessageType type) {
		this.type = type;
	}
	public String getMainMessage() {
		return mainMessage;
	}
	public void setMainMessage(String mainMessage) {
		this.mainMessage = mainMessage;
	}

	public String getHeaderMessage() {
		return headerMessage;
	}

	public void setHeaderMessage(String headerMessage) {
		this.headerMessage = headerMessage;
	}
	
	
}
