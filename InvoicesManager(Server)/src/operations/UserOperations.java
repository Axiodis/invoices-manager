package operations;

import exceptions.DbException;
import models.User;
import operations.crud.UserCrud;
import request.Message;
import request.MessageType;
import request.Request;
import request.RequestType;

public class UserOperations {
	
	public static Request registerUser(User user){
		try {
			UserCrud.createUser(user);
		}
		catch (DbException e) {
			return new Request(RequestType.Error, null, new Message(MessageType.Error, e.getMessage()));
		}
		catch (Exception e) {
			return new Request(RequestType.Error, null, new Message(MessageType.Error, "An unexpected error has occured!"));
		} 
		return new Request(RequestType.Succes, null, new Message(MessageType.Succes, "Register Succesful!"));
	}
	
	public static Request logInUser(User user) {

		User userToLogIn = UserCrud.findUserByName(user.getName());

		if (userToLogIn != null) {
			if (!userToLogIn.getPassword().equals(user.getPassword())) {
				return new Request(RequestType.Error, null, new Message(MessageType.Error, "Wrong credentials!"));
			}
		} else {
			return new Request(RequestType.Error, null, new Message(MessageType.Error, "No such user!"));
		}

		return new Request(RequestType.Succes, userToLogIn, new Message(MessageType.Succes, "LogIn Succesful!"));
	}
}
