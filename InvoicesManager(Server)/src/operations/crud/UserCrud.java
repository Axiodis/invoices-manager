package operations.crud;

import java.util.NoSuchElementException;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jinq.jpa.JinqJPAStreamProvider;

import exceptions.DbException;
import models.User;

public class UserCrud {

	static EntityManagerFactory emf = Persistence.createEntityManagerFactory("InvoicesManager(Server)");
	static EntityManager em = emf.createEntityManager();
	static JinqJPAStreamProvider streams = new JinqJPAStreamProvider(emf);

	public static void createUser(User user) throws DbException {
		try {
			em.getTransaction().begin();
			em.persist(user);
			em.getTransaction().commit();
		} catch (Exception e) {
			throw new DbException("Unexpected error: Unable to create invoice!",e);
		}
	}

	public static User findUserByName(String name) {

		Optional<User> searchedUser = streams.streamAll(em, User.class).where(u -> u.getName().equals(name))
				.findFirst();

		try {
			return searchedUser.get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public static User findUserById(int id) {

		return em.find(User.class, id);
	}

	
}
