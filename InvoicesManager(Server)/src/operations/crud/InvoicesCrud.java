package operations.crud;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jinq.jpa.JinqJPAStreamProvider;

import exceptions.DbException;
import models.Invoice;
import models.InvoiceStatus;

public class InvoicesCrud {
	static EntityManagerFactory emf = Persistence.createEntityManagerFactory("InvoicesManager(Server)");
    static EntityManager em =  emf.createEntityManager();
    static JinqJPAStreamProvider streams = new JinqJPAStreamProvider(emf);
    
    public static List<Invoice> getUserInvoices(int userId){
    	List<Invoice> invoices = streams
    			  .streamAll(em, Invoice.class)
    			  .where( i -> i.getUserId() == userId )
    			  .toList();
    	return invoices;
    }
    
    public static List<Invoice> getAdminInvoices(){
    	String pendingStatus = InvoiceStatus.PENDING.toString();
    	
    	List<Invoice> invoices = streams
    			  .streamAll(em, Invoice.class)
    			  .where( i -> i.getViewing() == 0 && i.getStatus().equals(pendingStatus) && i.getDiscount()==0)
    			  .limit(10)
    			  .toList();
    	return invoices;
    }
    
    public static List<Invoice> getSuperAdminInvoices(){
    	String pendingStatus = InvoiceStatus.PENDING.toString();
    	
    	List<Invoice> invoices = streams
    			  .streamAll(em, Invoice.class)
    			  .where( i -> i.getStatus().equals(pendingStatus) && i.getDiscount()>10)
    			  .limit(10)
    			  .toList();
    	return invoices;
    }
    
    public static void createInvoice(Invoice invoice) throws DbException{
    	try {
			em.getTransaction().begin();
			em.persist(invoice);
			em.getTransaction().commit();
		} catch (Exception e) {
			throw new DbException("Unexpected error: Unable to create invoice!",e);
		}
    }
    
    public static void updateInvoice(Invoice invoice) throws DbException{
    	try {
			em.getTransaction().begin();
			em.merge(invoice);
			em.getTransaction().commit();
		} catch (Exception e) {
			throw new DbException("Unexpected error: Unable to update invoice!",e);
		}
    }
}
