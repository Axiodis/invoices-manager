package operations;

import java.util.List;

import exceptions.DbException;
import models.Invoice;
import operations.crud.InvoicesCrud;
import request.Message;
import request.MessageType;
import request.Request;
import request.RequestType;

public class InvoiceOperations {
	public static Request getUserInvoices(int userId){
		
		Request response = new Request(RequestType.Succes);
		response.setContent(InvoicesCrud.getUserInvoices(userId));
		
		return response;
	}
	
	public static synchronized Request getAdminInvoices(){
		
		List<Invoice> invoices = InvoicesCrud.getAdminInvoices();
		
		for (Invoice invoice : invoices) {
			invoice.setViewing(1);
			try {
				InvoicesCrud.updateInvoice(invoice);
			} catch (DbException e) {
				return new Request(RequestType.Error, null, new Message(MessageType.Error, e.getMessage()));
			}
		}
		
		Request response = new Request(RequestType.Succes);
		response.setContent(invoices);
		
		return response;
	}
	
	public static Request getSuperAdminInvoices(){
		Request response = new Request(RequestType.Succes);
		response.setContent(InvoicesCrud.getSuperAdminInvoices());
		
		return response;
	}
	
	public static void releaseAdminInvoices (List<Invoice> adminInvoices){
		for (Invoice invoice : adminInvoices) {
			invoice.setViewing(0);
			try {
				InvoicesCrud.updateInvoice(invoice);
			} catch (DbException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Request addInvoice(Invoice invoice){
		try {
			InvoicesCrud.createInvoice(invoice);
		}
		catch (DbException e) {
			return new Request(RequestType.Error, null, new Message(MessageType.Error, e.getMessage()));
		}
		catch (Exception e) {
			return new Request(RequestType.Error, null, new Message(MessageType.Error, "An unexpected error has occured!"));
		} 
		return new Request(RequestType.Succes, null, new Message(MessageType.Succes, "Invoice added!"));
	}
	
	public static Request rejectInvoice(Invoice invoice){
		try {
			InvoicesCrud.updateInvoice(invoice);
		} catch (DbException e) {
			return new Request(RequestType.Error, null, new Message(MessageType.Error, e.getMessage()));
		}
		return new Request(RequestType.Succes, null, new Message(MessageType.Succes, "Invoice rejected!"));
	}
	
	public static Request approveInvoice(Invoice invoice){
		try {
			InvoicesCrud.updateInvoice(invoice);
		} catch (DbException e) {
			return new Request(RequestType.Error, null, new Message(MessageType.Error, e.getMessage()));
		}
		return new Request(RequestType.Succes, null, new Message(MessageType.Succes, "Invoice approved!"));
	}
}
