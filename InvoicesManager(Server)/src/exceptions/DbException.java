package exceptions;

public class DbException extends MyException {

	private static final long serialVersionUID = 1L;
	
	public DbException(String message) {
		super(message);
	}
	
	public DbException(String message, Exception baseException) {
		super(message,baseException);
	}
}
