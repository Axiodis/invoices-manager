package exceptions;

public class MyException extends Throwable {

	private static final long serialVersionUID = 1L;
	
	private String message;
	private Exception baseException;
	
	public MyException(String message) {
		this.setMessage(message);
	}

	public MyException(String message, Exception baseException) {
		this.setMessage(message);
		this.setBaseException(baseException);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Exception getBaseException() {
		return baseException;
	}

	public void setBaseException(Exception baseException) {
		this.baseException = baseException;
	}	
}
