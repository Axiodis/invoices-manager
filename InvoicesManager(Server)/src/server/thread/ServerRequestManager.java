package server.thread;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Invoice;
import models.User;
import operations.InvoiceOperations;
import operations.UserOperations;
import request.Request;
import request.RequestType;

public class ServerRequestManager {
	
	
	private static Map<RequestType, IRequestExecutable> commands;
	
	public static void initialize(){
		commands = new HashMap<>();
		
		commands.put(RequestType.Login, new IRequestExecutable() {
			@Override
			public void execute(ServerThread currentServer,Request request) throws IOException {
				currentServer.getServerOutputStream().writeObject(UserOperations.logInUser((User)request.getContent()));
			}
		});
		
		commands.put(RequestType.Register, new IRequestExecutable() {
			@Override
			public void execute(ServerThread currentServer,Request request) throws IOException {
				currentServer.getServerOutputStream().writeObject(UserOperations.registerUser((User)request.getContent()));
			}
		});
		
		commands.put(RequestType.GetUserInvoices, new IRequestExecutable() {
			@Override
			public void execute(ServerThread currentServer,Request request) throws IOException {
				currentServer.getServerOutputStream().writeObject(InvoiceOperations.getUserInvoices((int)request.getContent()));
			}
		});
		
		commands.put(RequestType.GetAdminInvoices, new IRequestExecutable() {
			@SuppressWarnings("unchecked")
			@Override
			public void execute(ServerThread currentServer,Request request) throws IOException {
				Request response = InvoiceOperations.getAdminInvoices();
				currentServer.setTempAdminInvoices((List<Invoice>) response.getContent());
				currentServer.getServerOutputStream().writeObject(response);
			}
		});
		
		commands.put(RequestType.GetSuperAdminInvoices, new IRequestExecutable() {
			@Override
			public void execute(ServerThread currentServer,Request request) throws IOException {
				currentServer.getServerOutputStream().writeObject(InvoiceOperations.getSuperAdminInvoices());
			}
		});
		
		commands.put(RequestType.AddInvoice, new IRequestExecutable() {
			@Override
			public void execute(ServerThread currentServer,Request request) throws IOException {
				currentServer.getServerOutputStream().writeObject(InvoiceOperations.addInvoice((Invoice)request.getContent()));
			}
		});
		
		commands.put(RequestType.RejectInvoice, new IRequestExecutable() {
			@Override
			public void execute(ServerThread currentServer,Request request) throws IOException {
				currentServer.getServerOutputStream().writeObject(InvoiceOperations.rejectInvoice((Invoice)request.getContent()));
				if(currentServer.getTempAdminInvoices()!=null){
					InvoiceOperations.releaseAdminInvoices(currentServer.getTempAdminInvoices());
				}
			}
		});
		
		commands.put(RequestType.ApproveInvoice, new IRequestExecutable() {
			@Override
			public void execute(ServerThread currentServer,Request request) throws IOException {
				currentServer.getServerOutputStream().writeObject(InvoiceOperations.approveInvoice((Invoice)request.getContent()));
				if(currentServer.getTempAdminInvoices()!=null){
					InvoiceOperations.releaseAdminInvoices(currentServer.getTempAdminInvoices());
				}
			}
		});
	}
	
	public static void run(ServerThread currentServer, Request request) throws IOException{
		commands.get(request.getType()).execute(currentServer, request);
	}
}
