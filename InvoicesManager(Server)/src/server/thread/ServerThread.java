package server.thread;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

import models.Invoice;
import operations.InvoiceOperations;
import request.Request;



public class ServerThread extends Thread {
	private Socket socket;
    private ObjectOutputStream serverOutputStream;
    private ObjectInputStream serverInputStream;
    private List<Invoice> tempAdminInvoices;

    public ServerThread(Socket socket) {
        this.socket = socket;
    }
    
    

    public ObjectOutputStream getServerOutputStream() {
		return serverOutputStream;
	}



	public void setServerOutputStream(ObjectOutputStream serverOutputStream) {
		this.serverOutputStream = serverOutputStream;
	}



	public List<Invoice> getTempAdminInvoices() {
		return tempAdminInvoices;
	}



	public void setTempAdminInvoices(List<Invoice> tempAdminInvoices) {
		this.tempAdminInvoices = tempAdminInvoices;
	}



	@Override
    public void run() {
    	
        try {
        	serverOutputStream = new ObjectOutputStream(socket.getOutputStream());
    		serverInputStream = new ObjectInputStream(socket.getInputStream());
        	
    		while (true) {
    			Request request = (Request) serverInputStream.readObject();

    			ServerRequestManager.run(this,request);
    			
    			
//    			switch (request.getType()) {
    			
//				case Register:
//					serverOutputStream.writeObject(UserOperations.registerUser((User)request.getContent()));
//					break;
					
//				case Login:
//					serverOutputStream.writeObject(UserOperations.logInUser((User)request.getContent()));
//					break;
					
//				case GetUserInvoices:
//					serverOutputStream.writeObject(InvoiceOperations.getUserInvoices((int)request.getContent()));
//					break;
					
//				case GetAdminInvoices:
//					Request response = InvoiceOperations.getAdminInvoices();
//					tempAdminInvoices = (List<Invoice>) response.getContent();
//					serverOutputStream.writeObject(response);
//					break;
					
//				case GetSuperAdminInvoices:
//					serverOutputStream.writeObject(InvoiceOperations.getSuperAdminInvoices());
//					break;
					
//				case AddInvoice:
//					serverOutputStream.writeObject(InvoiceOperations.addInvoice((Invoice)request.getContent()));
//					break;
					
//				case RejectInvoice:
//					serverOutputStream.writeObject(InvoiceOperations.rejectInvoice((Invoice)request.getContent()));
//					if(tempAdminInvoices!=null){
//						InvoiceOperations.releaseAdminInvoices(tempAdminInvoices);
//					}
//					break;
					
//				case ApproveInvoice:
//					serverOutputStream.writeObject(InvoiceOperations.approveInvoice((Invoice)request.getContent()));
//					if(tempAdminInvoices!=null){
//						InvoiceOperations.releaseAdminInvoices(tempAdminInvoices);
//					}
//					break;
					
//				default:
//					break;
//				}
        	}
            
        } catch (Exception e) {
        	e.printStackTrace();
            
        } finally {
        	InvoiceOperations.releaseAdminInvoices(tempAdminInvoices);
            try {
                serverOutputStream.close();
    			serverInputStream.close();
    			socket.close();
            } catch (Exception e) {
            	e.printStackTrace();
            }
        }
    }
}
