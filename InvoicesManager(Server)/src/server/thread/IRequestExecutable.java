package server.thread;

import java.io.IOException;

import request.Request;

public interface IRequestExecutable {
	void execute(ServerThread currentServer, Request request) throws IOException;
}
