package server;

import java.net.ServerSocket;

import server.thread.ServerRequestManager;
import server.thread.ServerThread;

public class Main {

	public static void main(String[] args) throws Exception {
        System.out.println("The Server is running.");
        ServerSocket listener = new ServerSocket(8000);
        ServerRequestManager.initialize();
        try {
            while (true) {
                new ServerThread(listener.accept()).start();
            }
        } finally {
            listener.close();
        }
    }
}
