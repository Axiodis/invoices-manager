package request;

public enum RequestType {
	Register, Login, GetUserInvoices, GetAdminInvoices, GetSuperAdminInvoices, RejectInvoice, ApproveInvoice, AddInvoice, Succes, Error
}
