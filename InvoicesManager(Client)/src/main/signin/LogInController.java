package main.signin;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import main.Client;
import main.Main;
import models.UserType;
import request.Message;
import request.MessageType;

public class LogInController {
	
	@FXML
    private TextField usernameField;

    @FXML
    private PasswordField passField;
	
	@FXML
	void register() throws IOException{
		Main.showRegister();
	}
	
	@FXML
	void logIn(ActionEvent event){
		System.out.println("Controller");
		Message message = Client.getClient().logInUser(usernameField.getText(), passField.getText());
		
		
		if(message.getType() == MessageType.Error){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle(message.getType().toString());
			alert.setHeaderText("");
			alert.setContentText(message.getMainMessage());
			alert.showAndWait();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle(message.getType().toString());
			alert.setHeaderText("");
			alert.setContentText(message.getMainMessage());
			alert.showAndWait();
			
			try {
				switch (UserType.valueOf(Client.getClient().getUser().getType())) {
				case COMMON:
					Main.showCommonUser();
					break;
				case ADMIN:
					Main.showAdmin();
					break;
				case SUPERADMIN:
					Main.showSuperAdmin();
					break;
				default:
					break;
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
