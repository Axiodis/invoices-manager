package main.signin;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.Client;
import main.Main;
import request.Message;
import request.MessageType;

public class RegisterController {
	
	@FXML
    private TextField usernameField;

    @FXML
    private PasswordField passField;

    @FXML
    private PasswordField confPassField;
    
    @FXML
	void logIn() throws IOException{
		Main.showLogIn();
	}

	@FXML
	void register(ActionEvent event) {
		
		if(!passField.getText().equals(confPassField.getText())){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error!");
			alert.setHeaderText("Password missmatch");
			alert.setContentText("The passwords do not match!");
			alert.showAndWait();
		}
		
		Message message = Client.getClient().registerUser(usernameField.getText(), passField.getText());
		
		if(message.getType() == MessageType.Error){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle(message.getType().toString());
			alert.setHeaderText("");
			alert.setContentText(message.getMainMessage());
			alert.showAndWait();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle(message.getType().toString());
			alert.setHeaderText("");
			alert.setContentText(message.getMainMessage());
			alert.showAndWait();
			
			try {
				Main.showLogIn();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    }
	
}
