package main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import com.sun.media.jfxmediaimpl.MediaDisposer.Disposable;

import models.Invoice;
import models.InvoiceStatus;
import models.User;
import models.UserType;
import request.Message;
import request.Request;
import request.RequestType;

public class Client implements Disposable{
	private static ObjectOutputStream clientOutputStream;
    private static ObjectInputStream clientInputStream;
    private static Client instance;
    private static User user;
    static Socket socket;
    
    private Client(){
    	try {
			clientOutputStream = new ObjectOutputStream(socket.getOutputStream());
			clientInputStream = new ObjectInputStream(socket.getInputStream());
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public static Client getClient(){
    	if(instance == null){
    		instance = new Client();
    	}
    	return instance;
    }
    
    public User getUser(){
    	return user;
    }
    
    public void setUser(User usr){
    	user = usr;
    }

	public static void Connect() throws UnknownHostException, IOException{
		socket = new Socket("localhost", 8000);
    }
    
    public Message registerUser(String name, String password){
    	User user = new User();
		user.setName(name);
		user.setPassword(password);
		user.setType(UserType.COMMON.toString());
		
		Request request = new Request(RequestType.Register, user);
		Request response = null;
		
		try {
			
			clientOutputStream.writeObject(request);
			response =  (Request) clientInputStream.readObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response.getMessage();
    }
    
    public Message logInUser(String name, String password){
    	User user = new User();
		user.setName(name);
		user.setPassword(password);
		
		Request request = new Request(RequestType.Login, user);
		Request response = null;
		
		try {
			
			clientOutputStream.writeObject(request);
			response =  (Request) clientInputStream.readObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		if(response.getContent()!=null){
			setUser((User)response.getContent());
		}
		
		return response.getMessage();
    }
    
    public Message addInvoice(String name, float value){
    	Invoice invoice = new Invoice();
    	invoice.setName(name);
    	invoice.setValue(value);
    	invoice.setUserId(user.getId());
    	invoice.setStatus(InvoiceStatus.PENDING.toString());
    	
    	Request request = new Request(RequestType.AddInvoice, invoice);
		Request response = null;
		
		try {
			
			clientOutputStream.writeObject(request);
			response =  (Request) clientInputStream.readObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response.getMessage();
    }
    
    public Message rejectInvoice(Invoice invoice){
    	invoice.setStatus(InvoiceStatus.REJECTED.toString());
    	invoice.setViewing(0);
    	
    	Request request = new Request(RequestType.RejectInvoice, invoice);
		Request response = null;
		
		try {
			
			clientOutputStream.writeObject(request);
			response =  (Request) clientInputStream.readObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response.getMessage();
    }
    
    public Message approveInvoice(Invoice invoice,int discount){
    	
    	if(discount<=10){
    		invoice.setStatus(InvoiceStatus.APPROVED.toString());
    	}
    	
    	invoice.setDiscount(discount);
    	invoice.setViewing(0);
    	
    	Request request = new Request(RequestType.ApproveInvoice, invoice);
		Request response = null;
		
		try {
			
			clientOutputStream.writeObject(request);
			response =  (Request) clientInputStream.readObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response.getMessage();
    }
    
    public Message approveInvoice(Invoice invoice){
    	
    	invoice.setStatus(InvoiceStatus.APPROVED.toString());
    	invoice.setViewing(0);
    	
    	Request request = new Request(RequestType.ApproveInvoice, invoice);
		Request response = null;
		
		try {
			
			clientOutputStream.writeObject(request);
			response =  (Request) clientInputStream.readObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response.getMessage();
    }
    
    
    
    @SuppressWarnings("unchecked")
	public List<Invoice> currentUserInvoices(){
		
		Request request = new Request(RequestType.GetUserInvoices, user.getId());
		Request response = null;
		
		try {
			
			clientOutputStream.writeObject(request);
			response =  (Request) clientInputStream.readObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		List<Invoice> content = (List<Invoice>)response.getContent();
		return content;
    }
    
    @SuppressWarnings("unchecked")
	public List<Invoice> currentAdminInvoices(){
		
		Request request = new Request(RequestType.GetAdminInvoices);
		request.setContent(user.getId());
		Request response = null;
		
		try {
			
			clientOutputStream.writeObject(request);
			response =  (Request) clientInputStream.readObject();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		List<Invoice> content = (List<Invoice>)response.getContent();
		return content;
    }
    
    @SuppressWarnings("unchecked")
   	public List<Invoice> currentSuperAdminInvoices(){
   		
   		Request request = new Request(RequestType.GetSuperAdminInvoices);
   		Request response = null;
   		
   		try {
   			
   			clientOutputStream.writeObject(request);
   			response =  (Request) clientInputStream.readObject();
   			
   		} catch (IOException e) {
   			e.printStackTrace();
   		} catch (ClassNotFoundException e) {
   			e.printStackTrace();
   		}
   		
   		List<Invoice> content = (List<Invoice>)response.getContent();
   		return content;
       }
    
    public void sendMessage(){
    	try {
			clientOutputStream.writeObject("Salut!");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

	@Override
	public void dispose() {
		try {
			clientInputStream.close();
			clientOutputStream.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
