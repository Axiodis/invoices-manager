package main.commonUser;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import main.Client;
import main.Main;
import request.Message;
import request.MessageType;

public class AddInvoiceController {

	@FXML
    private TextField nameField;
	
	@FXML
    private TextField valueField;
	
	@FXML
	void add(ActionEvent event){
		
		float value = 0;
		
		if(nameField.getText().isEmpty() || valueField.getText().isEmpty()){
			Main.displayDialogBox("Error!", "Please complete all fields!", MessageType.Error);
			return;
		}
		
		try {
			value = Float.parseFloat(valueField.getText());
			Message message = Client.getClient().addInvoice(nameField.getText(), value);
			Main.displayDialogBox(message.getType().toString(), message.getMainMessage(), message.getType());
			Main.showCommonUser();
		} catch (NumberFormatException e) {
			Main.displayDialogBox("Error!", "The value must pe a float number!", MessageType.Error);
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	void cancel(ActionEvent event){
		try {
			Main.showCommonUser();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
