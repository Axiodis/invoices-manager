package main.commonUser;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import main.Client;
import main.Main;
import models.Invoice;

public class CommonUserController implements Initializable {
	
	private ObservableList<Invoice> invoicesList;
	
	@FXML
	private TableView<Invoice> invoicesTable;
	@FXML
	private TableColumn<Invoice, Integer> id;
	@FXML
	private TableColumn<Invoice, String> name;
	@FXML
	private TableColumn<Invoice, String> status;
	@FXML
	private TableColumn<Invoice, Integer> discount;
	@FXML
	private TableColumn<Invoice, Float> value;
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		this.getInvoices();
		
		
		id.setCellValueFactory(new PropertyValueFactory<Invoice, Integer>("Id"));
		name.setCellValueFactory(new PropertyValueFactory<Invoice, String>("Name"));
		status.setCellValueFactory(new PropertyValueFactory<Invoice, String>("Status"));
		discount.setCellValueFactory(new PropertyValueFactory<Invoice, Integer>("Discount"));
		value.setCellValueFactory(new PropertyValueFactory<Invoice, Float>("Value"));
		
		invoicesTable.setItems(invoicesList);
	}
	
	private void getInvoices(){
		invoicesList = FXCollections.observableArrayList(Client.getClient().currentUserInvoices());
	}
	
	@FXML
	void addInvoice(ActionEvent event){
		try {
			Main.showCommonUserAddInvoice();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
