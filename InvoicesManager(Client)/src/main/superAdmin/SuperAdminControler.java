package main.superAdmin;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import main.Client;
import main.Main;
import models.Invoice;
import request.Message;
import request.MessageType;

public class SuperAdminControler implements Initializable {
	
	private ObservableList<Invoice> invoicesList;
	
	@FXML
	private TableView<Invoice> invoicesTable;
	@FXML
	private TableColumn<Invoice, Integer> id;
	@FXML
	private TableColumn<Invoice, String> name;
	@FXML
	private TableColumn<Invoice, String> status;
	@FXML
	private TableColumn<Invoice, Integer> discount;
	@FXML
	private TableColumn<Invoice, Float> value;
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
		id.setCellValueFactory(new PropertyValueFactory<Invoice, Integer>("Id"));
		name.setCellValueFactory(new PropertyValueFactory<Invoice, String>("Name"));
		status.setCellValueFactory(new PropertyValueFactory<Invoice, String>("Status"));
		discount.setCellValueFactory(new PropertyValueFactory<Invoice, Integer>("Discount"));
		value.setCellValueFactory(new PropertyValueFactory<Invoice, Float>("Value"));
		
		this.getInvoices();
		
	}

	private void getInvoices(){
		invoicesList = FXCollections.observableArrayList(Client.getClient().currentSuperAdminInvoices());
		invoicesTable.setItems(invoicesList);
	}
	
	@FXML
	void reject(ActionEvent event){
		
		Invoice selectedInvoice = invoicesTable.getSelectionModel().getSelectedItem();
		if(selectedInvoice == null){
			Main.displayDialogBox("Error", "You have to select an invoice first!", MessageType.Error);
			return;
		}
		
		Message message = Client.getClient().rejectInvoice(selectedInvoice);
		
		Main.displayDialogBox(message.getType().toString(), message.getMainMessage(), message.getType());
		
		this.getInvoices();
	}
	
	@FXML
	void approve(ActionEvent event){
		
		Invoice selectedInvoice = invoicesTable.getSelectionModel().getSelectedItem();
		if(selectedInvoice == null){
			Main.displayDialogBox("Error", "You have to select an invoice first!", MessageType.Error);
			return;
		}
		
		Message message = Client.getClient().approveInvoice(selectedInvoice);
		
		Main.displayDialogBox(message.getType().toString(), message.getMainMessage(), message.getType());
		
		this.getInvoices();
	}
}
