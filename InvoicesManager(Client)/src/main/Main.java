package main;
	
import java.io.IOException;
import java.net.UnknownHostException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import request.MessageType;


public class Main extends Application {
	
	private static Stage primaryStage;
	private static BorderPane mainLayout;
	
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Main.primaryStage = primaryStage;
			Main.primaryStage.setTitle("Invoices Manager");
			
			showMainView();
			showLogIn();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void stop(){
	    System.out.println("Stage is closing");
	}
	
	private void showMainView() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("view/MainView.fxml"));
		mainLayout = loader.load();
		Scene scene = new Scene(mainLayout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public static void showLogIn() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("signin/Login.fxml"));
		BorderPane logIn = loader.load();
		mainLayout.setCenter(logIn);
	}
	
	public static void showRegister() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("signin/Register.fxml"));
		BorderPane register = loader.load();
		mainLayout.setCenter(register);
	}
	
	public static void showCommonUser() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("commonUser/CommonUser.fxml"));
		AnchorPane commonUser = loader.load();
		mainLayout.setCenter(commonUser);
	}
	
	public static void showAdmin() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("admin/Admin.fxml"));
		AnchorPane admin = loader.load();
		mainLayout.setCenter(admin);
	}
	
	public static void showCommonUserAddInvoice() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("commonUser/AddInvoice.fxml"));
		AnchorPane commonUserAddInvoice = loader.load();
		mainLayout.setCenter(commonUserAddInvoice);
	}
	
	public static void showSuperAdmin() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("superAdmin/SuperAdmin.fxml"));
		AnchorPane admin = loader.load();
		mainLayout.setCenter(admin);
	}
	
	public static void displayDialogBox(String title,String content, MessageType type){
		Alert alert;
		if(type == MessageType.Error){
			alert = new Alert(AlertType.ERROR);
		} else{
			alert = new Alert(AlertType.INFORMATION);
		}
		alert.setTitle(title);
		alert.setHeaderText("");
		alert.setContentText(content);
		alert.showAndWait();
	}
	
	public static void main(String[] args) {
		try {
			Client.Connect();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		launch(args);
	}
}
