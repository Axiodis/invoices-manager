package models;

import java.io.Serializable;

public class Invoice implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private int discount;

	private String name;

	private String status;

	private int userId;

	private float value;
	
	private int viewing;

	public Invoice() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDiscount() {
		return this.discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public float getValue() {
		return this.value;
	}

	public void setValue(float value) {
		this.value = value;
	}
	
	public int getViewing() {
		return this.viewing;
	}

	public void setViewing(int value) {
		this.viewing = value;
	}

}